﻿using UnityEngine;

[RequireComponent (typeof (UnityEngine.AI.NavMeshAgent))]
[RequireComponent (typeof (Animator))]
[RequireComponent (typeof (LookAt))]
public class LocomotionSimpleAgent : MonoBehaviour {

	[SerializeField]
	bool pullCharacterTowardsAgent = false;
	[SerializeField]
	bool pullAgentTowardsCharacter = false;
	[SerializeField]
	[Range(0, 1)]
	float pullStrength = 1f;
	[SerializeField]
	[Range(0, 1)]
	float moveThreshold = 0.5f;

	Animator anim;
	UnityEngine.AI.NavMeshAgent agent;
	Vector2 smoothDeltaPosition = Vector2.zero;
	Vector2 velocity = Vector2.zero;
	LookAt lookAt;

	void Start () {
		anim = GetComponent<Animator> ();
		agent = GetComponent<UnityEngine.AI.NavMeshAgent> ();
		agent.updatePosition = false;
		agent.updateRotation = true;
		lookAt = GetComponent<LookAt>();
	}
	
	void Update () {
		Vector3 worldDeltaPosition = agent.nextPosition - transform.position;

		// Map 'worldDeltaPosition' to local space
		float dx = Vector3.Dot (transform.right, worldDeltaPosition);
		float dy = Vector3.Dot (transform.forward, worldDeltaPosition);
		Vector2 deltaPosition = new Vector2 (dx, dy);

		// Low-pass filter the deltaMove
		float smooth = Mathf.Min(1.0f, Time.deltaTime/0.15f);
		smoothDeltaPosition = Vector2.Lerp (smoothDeltaPosition, deltaPosition, smooth);

		// Update velocity if delta time is safe
		if (Time.deltaTime > 1e-5f)
			velocity = smoothDeltaPosition / Time.deltaTime;

		bool shouldMove = velocity.magnitude > moveThreshold && agent.remainingDistance > agent.radius;

		// Update animation parameters
		anim.SetBool("move", shouldMove);
		anim.SetFloat ("velx", velocity.x);
		anim.SetFloat ("vely", velocity.y);
		AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
        if (anim.GetBool("move"))
		{
			anim.speed = agent.velocity.magnitude / agent.speed; 
		}
        else
        {
			anim.speed = 1;
		}

		if (lookAt)
			lookAt.LookAtTargetPosition(agent.steeringTarget + transform.forward);


		// Pull character towards agent
		if (pullCharacterTowardsAgent && (worldDeltaPosition.magnitude > agent.radius))
			transform.position = agent.nextPosition - pullStrength*worldDeltaPosition;

		// Pull agent towards character
		if (pullAgentTowardsCharacter && (worldDeltaPosition.magnitude > agent.radius))
			agent.nextPosition = transform.position + pullStrength*worldDeltaPosition;
	}

	void OnAnimatorMove () {
		// Update postion to agent position
		transform.position = agent.nextPosition;

		// Update position based on animation movement using navigation surface height
//		Vector3 position = anim.rootPosition;
//		position.y = agent.nextPosition.y;
//		transform.position = position;
	}
}
