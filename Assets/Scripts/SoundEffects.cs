using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundEffects : MonoBehaviour
{
    [SerializeField]
    AudioClip[] clips;

    private AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Event defined in Animation file
    private void Step()
    {
        Debug.Log("Step Event");
        if (clips.Length > 0)
        {
            audioSource.PlayOneShot(clips[0]);
        }
        
    }
}
