﻿// ClickToMove.cs
using UnityEngine;

[RequireComponent (typeof (UnityEngine.AI.NavMeshAgent))]
public class ClickToMove : MonoBehaviour {

	public Transform waypoint;
	float angularSpeed;
	float rotationSpeed = 2f;
	RaycastHit hitInfo = new RaycastHit();
	UnityEngine.AI.NavMeshAgent agent;
	Quaternion waypointRotation;
	Quaternion quaternion;

	void Start () {
		agent = GetComponent<UnityEngine.AI.NavMeshAgent> ();
		angularSpeed = agent.angularSpeed;
	}
	void Update () {
		if(Input.GetMouseButtonDown(0) && (waypoint==null)) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray.origin, ray.direction, out hitInfo))
			{
				agent.destination = hitInfo.point;
				waypointRotation = Quaternion.identity;
				agent.angularSpeed = angularSpeed;
			}
		}
        else if (waypoint)
        {
			agent.destination = waypoint.position;
			waypointRotation = waypoint.rotation;
			waypoint = null;
			agent.angularSpeed = angularSpeed;
		}
        else
        {
			if (agent.remainingDistance <= 1.5f * agent.stoppingDistance)
			{
				agent.angularSpeed = 0;
				transform.rotation = Quaternion.Lerp(transform.rotation, waypointRotation, rotationSpeed * Time.deltaTime);
			}
		}
	}
}
